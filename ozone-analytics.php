<?php
/**
 * Plugin Name: Ozone Analytics  
 * Plugin URI: http://ozonegroup.co
 * Description: Weekly analytics report
 * Version: 1.0
 * Author: Peter Consuegra
 * Author URI: http://ozonegroup.co
 * License: GPL2
 */


add_action('admin_menu', 'ozone_analytics_create_menu');

function ozone_analytics_create_menu() {

	//create new top-level menu
	// add_menu_page('Theme page title', 'Pete Converter', 'manage_options', 'pete-export-options', 'testview',plugins_url('pete-converter/petefaceicon.png', dirname(__FILE__) ),6 );
	add_menu_page('OzoneAnalytics', 'Ozone Analytics', 'manage_options','ozone-analytics', 'ozone_analytics_settings_page', plugins_url('/images/logo_ozone_icon.png', __FILE__),6);

	//call register settings function
	//add_action( 'admin_init', 'register_ozone_multicurrency_plugin' );
	
	add_action( 'admin_init', 'register_ozone_analytics_plugin' );
}

function register_ozone_analytics_plugin() {
	//register our settings
	register_setting( 'ozone_analytics-settings-group', 'report_lang' );
	
}

function ozone_analytics_settings_page(){
	?>
	<div class="wrap">
		
	<h1>Ozone Analytics</h1>
	<h3>Edit email report settings</h3>
	<form method="post" action="options.php">
	    <?php settings_fields( 'ozone_analytics-settings-group' ); ?>
	    <?php do_settings_sections( 'ozone_analytics-settings-group' ); ?>
	    <table class="form-table">

		
	        <tr valign="top">
	        <th scope="row">Report lang</th>
	        <td>
			    
				<select name="report_lang" id="report_lang">
				  <option value="en" <?php if(get_option('report_lang') == "en") echo "selected"?>>English</option>
				  <option value="es" <?php if(get_option('report_lang') == "es") echo "selected"?>>Spanish</option>
				</select>

			</td>
	        </tr>
		
		
	    </table>
    
	    <?php submit_button(); ?>

	</form>
	
	
	<h3>Test email report</h3>
	
	<form method="post" action="/wp-admin/admin.php?page=ozone-analytics">
		 <input type="hidden" id="send_ozone_analytics_email" name="send_ozone_analytics_email" value="true">
		    
			<input type="text" id="email_test" placeholder="Email" name="email_test" style="width:400px">
			<?php submit_button("Send test email"); ?>

	</form>
	
	</div>
	
	
	<h3>Edit admin email</h3>
	
	<form method="post" action="/wp-admin/admin.php?page=ozone-analytics">
		 <input type="hidden" id="update_admin_email" name="update_admin_email" value="true">
		 
		  <?php wp_nonce_field( 'update_admin_email','admin_email' ); ?>
		 
		 <?php
		 
		 $select_tag_html = "<select name='user_id' id='cars'>";
		 
		 $blogusers = get_users( [ 'role__in' => [ 'administrator' ] ] );
		 // Array of WP_User objects.
		 foreach ( $blogusers as $user ) {
		    $select_tag_html .= "<option value='$user->ID'>$user->display_name</option>";
		 }
		 $select_tag_html .="</select>";
		 echo $select_tag_html;
		 ?>
		    <br /><br />
			<input type="text" id="new_email" placeholder="Email" name="new_email" style="width:400px">
			<?php submit_button("Save changes"); ?>

	</form>
	
	</div>
	
	<?php
	
	
	
}

//DEBUG LOGIC
if ( ! function_exists('write_log')) {
   function write_log ( $log )  {
      if ( is_array( $log ) || is_object( $log ) ) {
         error_log( print_r( $log, true ) );
      } else {
         error_log( $log );
      }
   }
}

function get_koko_realtime_page_views(){
	$since = strtotime( '-24 hours' );
	$counts = (array) get_option( 'koko_analytics_realtime_pageview_count', array() );
	$sum = 0;
	foreach ( $counts as $timestamp => $pageviews ) {
		if ( $timestamp > $since ) {
			$sum += $pageviews;
		}
	}
	return $sum;
}

function get_site_stats(){
	global $wpdb;
	$table_name = $wpdb->prefix . "koko_analytics_site_stats";
	$stats = $wpdb->get_results( "SELECT * FROM $table_name" );
	foreach ($stats as $row){ 
		write_log("date: $row->date -- visitors: $row->visitors -- pagevies: $row->pageviews");
	}
}

function hello_google_test(){
	//$realtime_page_view = get_realtime_pageview_count();
	//$plugin_path = plugin_dir_path(__FILE__);
	$page_views = get_koko_realtime_page_views();
	write_log("Google Test page views: $page_views");
	
	get_site_stats();
}

//add_action("init", "hello_google_test",10,1);

function set_mail_html_content_type() {
	return 'text/html';
}

function get_general_stats(){
	$previous_week = strtotime("-1 week +1 day");

	$start_week = strtotime("last sunday midnight",$previous_week);
	$end_week = strtotime("next saturday",$start_week);

	$start_week = date("Y-m-d",$start_week);
	$end_week = date("Y-m-d",$end_week);
	
	$current_date = date('Y-m-d');
	
	global $wpdb;
	$sql = "SELECT sum(visitors) as total_visitors, sum(pageviews) as total_pageviews FROM {$wpdb->prefix}koko_analytics_site_stats WHERE date >= '$start_week' and date <= '$current_date'";
	$results = $wpdb->get_results($sql, OBJECT );
	
	//write_log("SQL get_general_stats:");
	//write_log($sql);
	
	return $results[0];
}

function get_pages_stats(){
	
	$previous_week = strtotime("-1 week +1 day");

	$start_week = strtotime("last sunday midnight",$previous_week);
	$end_week = strtotime("next saturday",$start_week);

	$start_week = date("Y-m-d",$start_week);
	$end_week = date("Y-m-d",$end_week);
	
	$current_date = date('Y-m-d');

	global $wpdb;
	
	$sql = "SELECT post_stats.visitors as visitors, post_stats.pageviews as pageviews, posts.post_name as post_name, post_stats.date as register_date FROM {$wpdb->prefix}koko_analytics_post_stats post_stats inner join {$wpdb->prefix}posts posts on posts.id = post_stats.id WHERE date >= '$start_week' and date <= '$current_date' ORDER BY pageviews DESC";
	
	//write_log("SQL get_pages_stats:");
	//write_log($sql);
	
	$results = $wpdb->get_results($sql,OBJECT);
	return $results;
	
}

function get_referrer_stats(){
	$previous_week = strtotime("-1 week +1 day");

	$start_week = strtotime("last sunday midnight",$previous_week);
	$end_week = strtotime("next saturday",$start_week);

	$start_week = date("Y-m-d",$start_week);
	$end_week = date("Y-m-d",$end_week);
	
	$current_date = date('Y-m-d');

	global $wpdb;
	
	$sql = "SELECT referrer_stats.visitors as visitors, referrer_stats.pageviews as pageviews, referrer_urls.url as url, referrer_stats.date as register_date FROM {$wpdb->prefix}koko_analytics_referrer_stats referrer_stats inner join {$wpdb->prefix}koko_analytics_referrer_urls referrer_urls on referrer_urls.id = referrer_stats.id WHERE date >= '$start_week' and date <= '$current_date' ORDER BY pageviews DESC";
	//write_log("SQL get_referrer_stats:");
	//write_log($sql);
	
	$results = $wpdb->get_results($sql,OBJECT);
	
	return $results;
}

function send_analytics_email_logic($to){
	
	$general_stats = get_general_stats();
	$page_stats = get_pages_stats();
	$referrer_stats = get_referrer_stats();
	
	//LOGS
	/*
	write_log("total_visitors:");
	write_log($general_stats->total_visitors);
	
	write_log("total_pageviews:");
	write_log($general_stats->total_pageviews);
	
	write_log("page_stats:");
	write_log($page_stats);
	
	write_log("referrer_stats:");	
	write_log($referrer_stats);
	*/
	
	$site_url = get_site_url();
	$site_url = str_replace("http://","",$site_url);
	$site_url = str_replace("https://","",$site_url);
	
	$report_lang = get_option('report_lang');
	write_log("report_lang:");
	write_log($report_lang);
	
	if($report_lang == "es"){
		include( plugin_dir_path( __FILE__ ) . 'email_es.php');
		$subject = "¿Cuántas personas visitaron $site_url esta semana?";
		$message = get_email_es($site_url,$general_stats, $page_stats, $referrer_stats);
	}else{
		include( plugin_dir_path( __FILE__ ) . 'email_en.php');
		$subject = "How many users visited ozonegroup.co last week?";
		$message = get_email_en($site_url,$general_stats, $page_stats, $referrer_stats);
	}
	
	add_filter( 'wp_mail_content_type', 'set_mail_html_content_type' );
	wp_mail( $to, $subject, $message ); 
	remove_filter( 'wp_mail_content_type', 'set_mail_html_content_type' );
}

//TEST ANALYTICS REPORT
function send_ozone_analytics_email_test() {
	
	if(isset($_POST['send_ozone_analytics_email']) && ($_POST['send_ozone_analytics_email'] == "true")){ 
		
		if((isset($_POST['email_test'])) && ($_POST['email_test'] != "")){
			write_log("entro en test_email");
			send_analytics_email_logic($_POST['email_test']);
		}else{
			write_log("NO entro en test_email");
			$admin_email = get_option('admin_email');
			send_analytics_email_logic($admin_email);
		}
		
   	}
}

add_action('wp_loaded', 'send_ozone_analytics_email_test');


/*
Adtional cron schedule for testing
add_filter( 'cron_schedules', 'isa_add_every_three_minutes' );
function isa_add_every_three_minutes( $schedules ) {
    $schedules['every_three_minutes'] = array(
            'interval'  => 180,
            'display'   => __( 'Every 3 Minutes', 'textdomain' )
    );
    return $schedules;
}
*/

//Activate the CronJob when the plugin is activated or installed
register_activation_hook(__FILE__, 'my_activation');
 
function my_activation() {
    if (! wp_next_scheduled ( 'my_hourly_event' )) {
    wp_schedule_event(time(), 'hourly', 'my_hourly_event');
    }
}
 
add_action('my_hourly_event', 'do_this_hourly');
 
function do_this_hourly() {
    // do something every hour
	write_log("hello cronjob do_this_hourly");
	$admin_email = get_option('admin_email');
	send_analytics_email_logic($admin_email);
}


//CHANGE ADMIN EMAIL
function ozone_change_admin_email() {
	
	if ( ! empty( $_POST ) && check_admin_referer( 'update_admin_email', 'admin_email' ) ) {
	   // process form data, e.g. update fields
	   if(current_user_can('administrator')){
	   	 
   		$user_obj = get_user_by('id', $_POST['user_id']);
   		$new_email= $_POST['new_email'];
   		$new_email = sanitize_text_field( $new_email );
		
   		if(isset($new_email) && $new_email != ""){
   			global $wpdb;
   			$sql = "UPDATE {$wpdb->prefix}users SET user_email = '$new_email' WHERE ID = $user_obj->ID";
   			write_log("update users table");
   			write_log($sql);
   			$results = $wpdb->get_results($sql,OBJECT);
   			write_log($results);
			
			
   			$sql = "UPDATE {$wpdb->prefix}options SET option_value = '$new_email' WHERE option_name = 'admin_email'";
   			write_log("update options table");
   			write_log($sql);
   			$results = $wpdb->get_results($sql,OBJECT);
   			write_log($results);
   		}
		 
	   }
	}
	
}

add_action('wp_loaded', 'ozone_change_admin_email');




